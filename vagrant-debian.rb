Vagrant.configure(2) do |config|
  # install the Debian-provided Chef package
  config.vm.provision 'shell', inline: <<-SHELL
    sudo sed -i 's/ main$/ main contrib/' /etc/apt/sources.list
    sudo apt-get update
    sudo DEBIAN_FRONTEND=noninteractive apt-get -y install chef
  SHELL
end
