# General attributes
default['onlyoffice']['auto_assembly_interval'] = nil
default['onlyoffice']['cache_files_dir'] = nil # default
default['onlyoffice']['max_download_bytes'] = 104857600
default['onlyoffice']['max_file_size'] = '50MB'
default['onlyoffice']['download_package'] = true
default['onlyoffice']['version'] = '7.3.3'

# Database attributes
default['onlyoffice']['db']['host'] = 'localhost'
default['onlyoffice']['db']['name'] = 'onlyoffice'
default['onlyoffice']['db']['password'] = 'onlyoffice'
default['onlyoffice']['db']['port'] = 5432
default['onlyoffice']['db']['user'] = 'onlyoffice'

# JSON Web Token (JWT) attributes
default['onlyoffice']['jwt']['enable'] = false
default['onlyoffice']['jwt']['header'] = 'Authorization'
default['onlyoffice']['jwt']['in_body'] = false
default['onlyoffice']['jwt']['secret'] = 'secret'
# Deprecated attributes for JWT
default['onlyoffice']['token']['enable_request_inbox'] = false
default['onlyoffice']['token']['enable_request_outbox'] = false
default['onlyoffice']['token']['enable_browser'] = false
default['onlyoffice']['token']['secret_inbox'] = nil
default['onlyoffice']['token']['secret_outbox'] = nil
default['onlyoffice']['token']['secret_session'] = nil

# NGINX attributes
default['onlyoffice']['nginx']['secure_link_secret'] = 'verysecretstring'
default['onlyoffice']['nginx']['ssl_certificate_key'] = nil
default['onlyoffice']['nginx']['ssl_certificate'] = nil
default['onlyoffice']['nginx']['ssl_dhparam'] = nil # opt
default['onlyoffice']['nginx']['ssl_password_file'] = nil
default['onlyoffice']['nginx']['ssl_stapling_certificate'] = nil # opt
default['onlyoffice']['nginx']['ssl_verify_client'] = false
