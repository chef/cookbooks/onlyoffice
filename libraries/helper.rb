#
# Cookbook:: onlyoffice
# Library:: helper
#
# Copyright:: 2021-2023, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

module OnlyOffice
  # Helper functions for the OnlyOffice cookbook
  module Helper
    module_function

    def onlyoffice_download_deb_url(version)
      "https://download.onlyoffice.com/install/documentserver/linux/onlyoffice-documentserver-ee_#{version}_amd64.deb"
    end

    def onlyoffice_use_supervisor?
      wanted = node['onlyoffice']['version'].sub(/~.*$/, '')
      Gem::Version.new(wanted) < Gem::Version.new('7.3.0')
    end

    def onlyoffice_notifies_main_service
      if onlyoffice_use_supervisor?
        notifies :reload, 'service[supervisor]'
      else
        notifies :restart, 'service[ds-docservice]'
      end
    end
  end
end

Chef::DSL::Recipe.include OnlyOffice::Helper
Chef::Resource.include OnlyOffice::Helper
