#
# Cookbook:: onlyoffice
# Resource:: onlyoffice_dependencies
#
# Copyright:: 2021-2023, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'onlyoffice_dependencies'

action :install do
  package 'default-mysql-server'
  package 'libgconf-2-4'
  package 'nginx-extras'
  package 'postgresql'
  package 'postgresql-client'
  package 'pwgen'
  package 'rabbitmq-server'
  package 'redis-server'
  package 'supervisor' if onlyoffice_use_supervisor?
  package 'ttf-mscorefonts-installer'
  package 'xvfb'

  package 'default-mysql-client'
  package 'libasound2'
  package 'libcairo2'
  package 'libgtk-3-0'
  package 'libxss1'
  package 'libxtst6'
  package 'mariadb-client'
end
