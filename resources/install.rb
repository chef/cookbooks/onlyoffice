#
# Cookbook:: onlyoffice
# Resource:: onlyoffice_install
#
# Copyright:: 2021-2023, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'onlyoffice_install'

property :checksum, String, desired_state: false
property :dbhost, String, default: 'localhost', desired_state: false
property :dbname, String, default: 'onlyoffice', desired_state: false
property :dbpassword, String, required: true, desired_state: false
property :dbport, [Integer, String], default: 5432, desired_state: false
property :dbuser, String, default: 'onlyoffice', desired_state: false
property :download_package, [true, false], default: true, desired_state: false
property :force, [true, false], default: false, desired_state: false
property :url, String, desired_state: false
property :version, String, required: true

load_current_value do
  installed_versions = shell_out!('apt list onlyoffice-documentserver-ee')
                       .stdout.split(/\n/).select { |l| l.match(/(installed|upgradable)/) }
  if installed_versions.nil? || installed_versions.empty?
    version 'not installed'
  elsif installed_versions[0] =~ /upgradable/
    version installed_versions[0].split(/[ \]]/)[5]
  else
    version installed_versions[0].split[1]
  end
end

action :install do
  converge_if_changed do
    cache_path = Chef::Config[:file_cache_path]
    download_url = new_resource.url || onlyoffice_download_deb_url(new_resource.version)
    archive_path = "#{cache_path}/onlyoffice-documentserver-#{new_resource.version}.deb"

    onlyoffice_dependencies 'dependencies'

    remote_file archive_path do
      source download_url
      checksum new_resource.checksum if new_resource.checksum
      mode '0644'
      action :create_if_missing
      only_if { new_resource.download_package }
    end

    postgresql_server_install 'PostgreSQL Server' do
      setup_repo false
      version '11'
      only_if { new_resource.dbhost.eql? 'localhost' }
    end

    postgresql_user new_resource.dbuser do
      host new_resource.dbhost unless new_resource.dbhost.eql? 'localhost'
      password new_resource.dbpassword
      port new_resource.dbport
    end

    postgresql_database new_resource.dbname do
      host new_resource.dbhost unless new_resource.dbhost.eql? 'localhost'
      owner new_resource.dbuser
      port new_resource.dbport
      template 'template0'
    end

    execute 'configure debconf' do
      command "echo 'onlyoffice onlyoffice/db-pwd password #{new_resource.dbpassword}' | sudo debconf-set-selections"
      sensitive true
    end

    if new_resource.download_package
      execute 'install OnlyOffice package' do
        cwd cache_path
        command "dpkg -i '#{archive_path}'"
      end
    else
      package 'onlyoffice-documentserver-ee' do
        version new_resource.version
      end
    end
  end
end

action_class do
  include OnlyOffice::Helper
end
